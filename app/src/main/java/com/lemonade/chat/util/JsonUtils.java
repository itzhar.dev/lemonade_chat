package com.lemonade.chat.util;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.InputStreamReader;
import java.lang.reflect.Type;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class JsonUtils {
    private final Context context;
    private final Gson gson;

    @Inject
    public JsonUtils(Context context) {
        this.context = context;
        this.gson = new GsonBuilder().create();
    }

    public  <T> T loadJSON(int rawResourceId, Type type) {
        InputStreamReader streamReader =
                new InputStreamReader(context.getResources().openRawResource(rawResourceId));
        JsonReader jr = new JsonReader(streamReader);
        return gson.fromJson(jr, type);
    }
}
