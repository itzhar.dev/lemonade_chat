package com.lemonade.chat.adapters.view_holders

import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.ContextThemeWrapper
import android.view.View
import com.lemonade.chat.R
import com.lemonade.chat.adapters.MassegesAdapter
import com.lemonade.chat.model.Message
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_messege_out.*

class MassegeOutViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
    override val containerView: View?
        get() = itemView

    fun bind(item: Message, position: Int, listener: MassegesAdapter.EditAnswerListener) {
        message_out_text.text = item.text
        menu.setOnClickListener {
            val wrapper = ContextThemeWrapper(itemView.context, R.style.PopupMenuStyle)
            val popup = PopupMenu(wrapper, menu)
            popup.menuInflater.inflate(R.menu.menu, popup.menu)
            popup.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.menu_edit_answer -> {
                        listener.goBackToStep(item, position)
                        return@setOnMenuItemClickListener true
                    }
                    else -> {
                        return@setOnMenuItemClickListener false
                    }
                }
            }
            popup.show()
        }
    }
}
