package com.lemonade.chat.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lemonade.chat.R
import com.lemonade.chat.model.MassegeType
import com.lemonade.chat.adapters.view_holders.MassegeInViewHolder
import com.lemonade.chat.adapters.view_holders.MassegeOutViewHolder
import com.lemonade.chat.model.Message
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import com.lemonade.chat.core.Preferences
import com.lemonade.chat.model.ConversationState
import io.realm.Realm
import io.realm.RealmResults

class MassegesAdapter(val preferences: Preferences, val updateUiListener: UpdateUiListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var lastPosition = -1
    private var items = mutableListOf<Message>()

    fun setItems(items: RealmResults<Message>?) {
        val list = mutableListOf<Message>()
        items?.forEach { list.add(it) }
        if (this.items.isEmpty() || items == null) {
            this.items = list
            lastPosition = -1
        }
        this.items = list
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (items[position].senderId == "lemonade") MassegeType.IN.ordinal else MassegeType.OUT.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (MassegeType.values()[viewType]) {
            MassegeType.IN -> {
                MassegeInViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_messege_in, parent, false))
            }
            MassegeType.OUT -> {
                MassegeOutViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_messege_out, parent, false))
            }
        }
    }


    val listener = object : EditAnswerListener {
        override fun goBackToStep(message: Message, reverstToPosition: Int) {
            preferences.updateConversationState(ConversationState.WAIT_FOR_ACTION)
            val realm = Realm.getDefaultInstance()
            updateUiListener.initActionViews(message.prevStep!!)
            realm.executeTransaction { _ ->
                val rows = realm.where(Message::class.java).greaterThanOrEqualTo("timestamp", message.timestamp!!).findAll()
                rows.deleteAllFromRealm()
            }
            lastPosition = reverstToPosition
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MassegeInViewHolder) {
            holder.bind(items[position])
        } else if (holder is MassegeOutViewHolder) {
            holder.bind(items[position], position, listener)
        }
        setAnimation(holder.itemView, position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val anim = ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
            anim.duration = 500
            viewToAnimate.startAnimation(anim)
            lastPosition = position
        }
    }


    interface EditAnswerListener {
        fun goBackToStep(message: Message, reverstToPosition: Int)
    }

    interface UpdateUiListener {
        fun initActionViews(currentStep: String)
    }
}
