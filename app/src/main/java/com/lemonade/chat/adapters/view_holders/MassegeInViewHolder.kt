package com.lemonade.chat.adapters.view_holders

import android.support.v7.widget.RecyclerView
import android.view.View
import com.lemonade.chat.model.Message
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_messege_in.*

class MassegeInViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
    override val containerView: View?
        get() = itemView

    fun bind(item: Message) {
        message_in_text.text = item.text
    }
}
