package com.lemonade.chat

import android.app.Application
import android.support.multidex.MultiDex
import com.lemonade.chat.inject.component.ApplicationComponent
import com.lemonade.chat.inject.component.DaggerApplicationComponent
import com.lemonade.chat.inject.modules.AppModule
import io.realm.Realm
import io.realm.RealmConfiguration
import timber.log.Timber

class LemonadeApplication : Application() {

    private var applicationComponent: ApplicationComponent? = null

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        initRealmConfiguration()
        //clearRealm()
        Timber.plant(Timber.DebugTree())

        applicationComponent = DaggerApplicationComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    private fun initRealmConfiguration() {
        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(realmConfiguration)
    }

    private fun clearRealm() {
        val realm = Realm.getDefaultInstance()
        try {
            realm.beginTransaction()
            realm.deleteAll()
            realm.commitTransaction()
        } finally {
            realm.close()
        }
    }
}
