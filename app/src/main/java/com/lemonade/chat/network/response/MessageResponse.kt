package com.lemonade.chat.network.response


class MessageResponse {
    var messageId: String? = null
    var timestamp: Long? = null
    var senderId: String? = null
    var senderName: String? = null
    var text: String? = null
}