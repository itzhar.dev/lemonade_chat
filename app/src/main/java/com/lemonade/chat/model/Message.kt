package com.lemonade.chat.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import org.joda.time.DateTime

@RealmClass
open class Message : RealmObject() {
    @PrimaryKey
    var messageId: String? = null
    var timestamp: Long? = null
    var senderId: String? = null
    var senderName: String? = null
    var text: String? = null
    var currentStep: String? = null
    var prevStep: String? = null

    fun getTimeStamp(): DateTime {
        return DateTime(timestamp)
    }

    override fun equals(other: Any?): Boolean {
        return (other as Message).messageId == this.messageId
    }

    override fun hashCode(): Int {
        var result = messageId?.hashCode() ?: 0
        result = 31 * result + (timestamp?.hashCode() ?: 0)
        result = 31 * result + (senderId?.hashCode() ?: 0)
        result = 31 * result + (senderName?.hashCode() ?: 0)
        result = 31 * result + (text?.hashCode() ?: 0)
        result = 31 * result + (currentStep?.hashCode() ?: 0)
        return result
    }
}