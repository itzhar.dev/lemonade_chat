package com.lemonade.chat.model

enum class ConversationState {
    //WAIT_FOR_ACTION - in step that we wait for bot complete his conversation (more then 1 messaege)
    //we want to ignore user input, because he still dont know what the bot wants
    WAIT_FOR_ACTION,

    SET_NAME,
    SET_MOBILE,
    TOU_AGREEMENT,
    EXIT_OR_RESTART,
    FINISH
}