package com.lemonade.chat.fragments

import android.content.Context
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lemonade.chat.adapters.MassegesAdapter
import com.lemonade.chat.inject.component.FragmentComponent
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_main.*
import com.lemonade.chat.core.LemonadeFragment
import com.lemonade.chat.core.Preferences
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import com.lemonade.chat.data.repositories.LemonadeRepository
import timber.log.Timber
import javax.inject.Inject
import android.text.TextUtils
import com.jakewharton.rxbinding2.widget.RxTextView
import com.lemonade.chat.R
import com.lemonade.chat.adapters.SpacesItemDecoration
import com.lemonade.chat.model.ConversationState
import android.text.InputType
import android.view.inputmethod.InputMethodManager
import io.realm.Realm

class MainFragment : LemonadeFragment() {

    @Inject
    lateinit var lemonadeRepository: LemonadeRepository
    @Inject
    lateinit var preferences: Preferences

    private lateinit var massegesAdapter: MassegesAdapter

    var disposable = CompositeDisposable()
    var itemsViewAdapter: com.lemonade.chat.adapters.MassegesAdapter? = null
    private var news_barHeight: Float = 0.toFloat()

    val listener = object : MassegesAdapter.UpdateUiListener{
        override fun initActionViews(currentStep: String) {
            this@MainFragment.initActionViews(ConversationState.valueOf(currentStep))
        }
    }

    override fun inject(component: FragmentComponent) {
        component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        //in real project i probably use presenter for those functions,
        //but for this code example i wrote them in fragment.
        //my apologies
        initActionViews(preferences.getCurrentConversationState())
        loadMessages()
        observeSendButtonAndEnable()
        setComposerClickListener()
        setLeftButtonClickListener()
        setRightButtonClickListener()
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        layoutManager.reverseLayout = false
        layoutManager.stackFromEnd = true
        massegesAdapter = MassegesAdapter(preferences, listener)
        recycler_view.layoutManager = layoutManager
        val massegesAdapter = massegesAdapter
        recycler_view.addItemDecoration(SpacesItemDecoration(20))
        recycler_view.adapter = massegesAdapter
    }

    private fun loadMessages() {
        //every time new message inserted to realm db,
        // UI will notified
        lemonadeRepository.getMasseges()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.isEmpty()) {
                        //in the first run when there is no messages, we start bot conversation
                        lemonadeRepository.step1FirstMessage()
                                .flatMap { lemonadeRepository.step1SecondMessage() }
                                .subscribe()
                    } else {
                        massegesAdapter.setItems(it)
                        // i build the data so the last message for every step is carring the current action.
                        // till the last message if user send message nothing happend
                        val lastMessage = it[it.size - 1]
                        if (!TextUtils.isEmpty(lastMessage?.currentStep)) {
                            preferences.updateConversationState(lastMessage?.currentStep!!)
                            val currentState = ConversationState.valueOf(lastMessage.currentStep!!)
                            initActionViews(currentState)
                        }
                    }
                }, { Timber.e(it) })
    }

    private fun initActionViews(currentState: ConversationState) {
        //all messages are chahed in realm DB, so user can open the app
        //in the middle of conversation. UI need to set correctly
        when (currentState) {
            ConversationState.SET_NAME -> {
                right_button.visibility = View.GONE
                left_button.visibility = View.GONE
                devider.visibility = View.GONE

                composer.visibility = View.VISIBLE
                send_button.visibility = View.VISIBLE
                composer.requestFocus()
                composer.inputType = InputType.TYPE_CLASS_TEXT
            }
            ConversationState.SET_MOBILE -> {
                composer.visibility = View.VISIBLE
                send_button.visibility = View.VISIBLE
                composer.inputType = InputType.TYPE_CLASS_NUMBER
                composer.requestFocus()
                right_button.visibility = View.GONE
                left_button.visibility = View.GONE
                devider.visibility = View.GONE
            }
            ConversationState.TOU_AGREEMENT -> {
                //in real worl i swipe them animated...
                right_button.visibility = View.VISIBLE
                left_button.visibility = View.VISIBLE
                devider.visibility = View.VISIBLE

                left_button.text = "Yes"
                right_button.text = "No"

                composer.visibility = View.INVISIBLE
                send_button.visibility = View.INVISIBLE
            }
            ConversationState.EXIT_OR_RESTART -> {
                right_button.visibility = View.VISIBLE
                left_button.visibility = View.VISIBLE
                devider.visibility = View.VISIBLE

                left_button.text = "Restart"
                right_button.text = "Exit"

                composer.visibility = View.INVISIBLE
                send_button.visibility = View.INVISIBLE
            }
            ConversationState.FINISH -> {
                composer.visibility = View.INVISIBLE
                send_button.visibility = View.INVISIBLE

                left_button.visibility = View.VISIBLE
                left_button.text = "Restart"
            }
            else -> {

            }
        }
    }

    private fun observeSendButtonAndEnable() {
        send_button.isEnabled = false
        RxTextView.afterTextChangeEvents(composer)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val tintColor = if (composer.length() == 0) ContextCompat.getColor(context!!, R.color.send_button_disable) else ContextCompat.getColor(context!!, R.color.send_button_enable)
                    send_button.setColorFilter(tintColor, PorterDuff.Mode.MULTIPLY)
                    send_button.isEnabled = composer.length() > 0
                }, {

                })
    }

    private fun setComposerClickListener() {
        send_button.setOnClickListener {
            val currentConversationState = preferences.getCurrentConversationState()
            if (currentConversationState != ConversationState.WAIT_FOR_ACTION) {//see comment inside ConversationState
                val userMessage = composer.text.toString()
                lemonadeRepository.sendMessage(composer.text.toString(), preferences.getCurrentConversationState())
                composer.setText("")
                preferences.updateConversationState(ConversationState.WAIT_FOR_ACTION)

                when (currentConversationState) {
                    ConversationState.SET_NAME -> {
                        lemonadeRepository.step2FirstMessage(userMessage)
                                .flatMap { lemonadeRepository.step2SecondMessage() }
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({

                                })
                    }
                    ConversationState.SET_MOBILE -> {

                        //hide keyboard. next step is action buttons
                        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(view!!.windowToken, 0)

                        lemonadeRepository.step3FirstMessage()
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({

                                })
                    }
                    else -> {
                    }
                }
            }
        }
    }

    private fun setLeftButtonClickListener() {
        left_button.setOnClickListener {
            val currentConversationState = preferences.getCurrentConversationState()
            when (currentConversationState) {//yes - user agree
                ConversationState.TOU_AGREEMENT -> {
                    lemonadeRepository.sendMessage("Yes", preferences.getCurrentConversationState())
                    right_button.visibility = View.INVISIBLE
                    left_button.visibility = View.INVISIBLE
                    devider.visibility = View.INVISIBLE
                    lemonadeRepository.startStep4()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe()
                }
                ConversationState.FINISH,
                ConversationState.EXIT_OR_RESTART -> {
                    //user click on restart
                    clearRealm()
                    massegesAdapter.setItems(null)
                    preferences.updateConversationState(ConversationState.WAIT_FOR_ACTION)

                    right_button.visibility = View.GONE
                    left_button.visibility = View.GONE
                    devider.visibility = View.GONE

                    composer.visibility = View.VISIBLE
                    send_button.visibility = View.VISIBLE

                    composer.inputType = InputType.TYPE_CLASS_TEXT
                }
                else -> {
                }
            }
        }
    }

    private fun setRightButtonClickListener() {
        right_button.setOnClickListener {
            right_button.visibility = View.GONE
            left_button.visibility = View.GONE
            devider.visibility = View.GONE
            if (preferences.getCurrentConversationState() == ConversationState.TOU_AGREEMENT) {
                lemonadeRepository.sendMessage("No", preferences.getCurrentConversationState())
            } else {
                lemonadeRepository.sendMessage("Exit", preferences.getCurrentConversationState())
            }
            preferences.updateConversationState(ConversationState.FINISH)

            lemonadeRepository.lastStepMessage()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({

                    })
        }
    }

    private fun clearRealm() {
        val realm = Realm.getDefaultInstance()
        try {
            realm.beginTransaction()
            realm.deleteAll()
            realm.commitTransaction()
        } finally {
            realm.close()
        }
    }
}
