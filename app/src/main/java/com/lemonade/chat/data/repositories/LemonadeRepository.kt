package com.lemonade.chat.data.repositories


import android.os.SystemClock
import com.lemonade.chat.model.ConversationState
import com.lemonade.chat.model.Message
import io.reactivex.Flowable
import io.realm.RealmResults
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LemonadeRepository @Inject
constructor(var chatRepository: ChatRepository) {
    fun step1FirstMessage(): Flowable<Message> {
        return chatRepository.step1FirstMessage()
    }

    fun step1SecondMessage(): Flowable<Message> {
        return chatRepository.step1SecondMessage()
    }

    fun step2FirstMessage(userName: String): Flowable<Message> {
        return chatRepository.step2FirstMessage(userName)
    }

    fun step2SecondMessage(): Flowable<Message> {
        return chatRepository.step2SecondMessage()
    }

    fun step3FirstMessage(): Flowable<Message> {
        return chatRepository.step3FirstMessage()
    }

    fun startStep4(): Flowable<Message> {
        return chatRepository.step4FirstMessage()
                .flatMap { chatRepository.step4SecondMessage() }
                .flatMap { chatRepository.step4ThirdMessage() }
    }

    fun lastStepMessage(): Flowable<Message> {
        return chatRepository.lastStepMessage()
    }

    fun getMasseges(): Flowable<RealmResults<Message>> {
        return chatRepository.getMessages()!!
    }

    fun sendMessage(messageText: String, answerForStep: ConversationState) {
        val msg = Message()
        msg.text = messageText
        msg.senderId = "user"
        msg.senderName = "User"
        val currentTimeMillis = System.currentTimeMillis()
        msg.timestamp = currentTimeMillis
        msg.messageId = currentTimeMillis.toString()
        msg.prevStep = answerForStep.name

        chatRepository.sendMessage(msg)
    }
}