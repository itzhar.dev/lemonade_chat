package com.lemonade.chat.data.repositories

import com.lemonade.chat.data.interfaces.LemonadeDataSource
import com.lemonade.chat.data.local.LemonadeLocalDataSource
import com.lemonade.chat.data.remote.LemonadeMockDataSource
import com.lemonade.chat.model.Message
import io.reactivex.Flowable
import io.realm.RealmResults
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ChatRepository @Inject constructor(jsonUtils: com.lemonade.chat.util.JsonUtils) : LemonadeDataSource {

    var mockDataSource = LemonadeMockDataSource(jsonUtils)
    var localDataSource = LemonadeLocalDataSource()

    override fun getMessages(): Flowable<RealmResults<Message>>? {
        return localDataSource.getMessages()
    }

    override fun step1FirstMessage(): Flowable<Message> {
        return mockDataSource.step1FirstMessage()
    }

    override fun step1SecondMessage(): Flowable<Message> {
        return mockDataSource.step1SecondMessage()
    }

    override fun step1Input(): Flowable<Message> {
        return mockDataSource.step1Input()
    }

    override fun step2FirstMessage(userName: String): Flowable<Message> {
        return mockDataSource.step2FirstMessage(userName)
    }

    override fun step2SecondMessage(): Flowable<Message> {
        return mockDataSource.step2SecondMessage()
    }

    override fun step3FirstMessage(): Flowable<Message> {
        return mockDataSource.step3FirstMessage()
    }

    override fun step4FirstMessage(): Flowable<Message> {
        return mockDataSource.step4FirstMessage()
    }

    override fun step4SecondMessage(): Flowable<Message> {
        return mockDataSource.step4SecondMessage()
    }

    override fun step4ThirdMessage(): Flowable<Message> {
        return mockDataSource.step4ThirdMessage()
    }

    override fun lastStepMessage(): Flowable<Message> {
        return mockDataSource.lastStepMessage()
    }

    override fun saveMessage(msg: Message) {

    }

    override fun sendMessage(msg: Message) {
        mockDataSource.sendMessage(msg)
    }
}
