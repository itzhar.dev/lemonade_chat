package com.lemonade.chat.data.interfaces

import com.lemonade.chat.model.Message
import io.reactivex.Flowable
import io.realm.RealmResults

interface LemonadeDataSource {
    fun getMessages(): Flowable<RealmResults<Message>>?
    fun saveMessage(msg:Message)
    fun sendMessage(msg:Message)

    fun step1FirstMessage(): Flowable<Message>
    fun step1SecondMessage(): Flowable<Message>
    fun step1Input(): Flowable<Message>

    fun step2FirstMessage(userName: String): Flowable<Message>
    fun step2SecondMessage(): Flowable<Message>

    fun step3FirstMessage(): Flowable<Message>

    fun step4FirstMessage(): Flowable<Message>
    fun step4SecondMessage(): Flowable<Message>
    fun step4ThirdMessage(): Flowable<Message>

    fun lastStepMessage(): Flowable<Message>
}
