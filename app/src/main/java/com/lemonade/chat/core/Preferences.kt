package com.lemonade.chat.core

import android.content.Context
import android.content.SharedPreferences
import com.lemonade.chat.model.ConversationState
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Preferences @Inject
constructor(context: Context) {

    companion object {
        const val SHARED_PREFERENCES_FILE_NAME = "lemonade_preferences"
        const val CONVERSATION_STATE = "cpnversation_state"
    }

    private val sharedPreferences: SharedPreferences

    init {
        sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)
    }

    fun getCurrentConversationState(): ConversationState {
        return ConversationState.valueOf(sharedPreferences.getString(CONVERSATION_STATE, ConversationState.WAIT_FOR_ACTION.name))
    }

    fun updateConversationState(state: ConversationState) {
        sharedPreferences.edit().putString(CONVERSATION_STATE, state.name).apply()
    }

    fun updateConversationState(state: String) {
        sharedPreferences.edit().putString(CONVERSATION_STATE, state).apply()
    }
}