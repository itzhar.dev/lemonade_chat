package com.lemonade.chat.core

import android.os.Bundle
import android.support.annotation.AnimRes
import android.support.v4.app.Fragment
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.lemonade.chat.R
import com.lemonade.chat.inject.component.DaggerFragmentComponent
import com.lemonade.chat.inject.component.FragmentComponent
import com.lemonade.chat.inject.modules.AppModule

abstract class LemonadeFragment : Fragment() {
    @AnimRes private var nextExitAnimation = -1
    @AnimRes private var nextEnterAnimation = -1

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {

        if (nextEnterAnimation != -1 && enter) {
            try {
                return AnimationUtils.loadAnimation(context, nextEnterAnimation)
            } finally {
                nextEnterAnimation = -1
            }
        }

        if (nextExitAnimation != -1 && !enter) {
            try {
                return AnimationUtils.loadAnimation(context, nextExitAnimation)
            } finally {
                nextExitAnimation = -1
            }
        }

        return super.onCreateAnimation(transit, enter, nextAnim)
    }

    fun overrideNextEnterAnimation(@AnimRes animation: Int) {
        this.nextEnterAnimation = animation
    }

    fun overrideNextExitAnimation(@AnimRes animation: Int) {
        this.nextExitAnimation = animation
    }

    protected abstract fun inject(component: FragmentComponent)

    private var component: FragmentComponent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (component == null) {
            component = DaggerFragmentComponent.builder()
                    .appModule(AppModule(activity!!.application))
                    .build()

        }
        inject(component!!)
    }


    fun add(fragment: Fragment, addToBackStack: Boolean) {
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.container, fragment)
        if (addToBackStack) {
            transaction?.addToBackStack(fragment.javaClass.simpleName)
        }
        transaction?.commit()
    }

    fun replace(fragment: Fragment, addToBackStack: Boolean) {
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.container, fragment)
        if (addToBackStack) {
            transaction?.addToBackStack(fragment.javaClass.simpleName)
        }
        transaction?.commit()
    }
}
