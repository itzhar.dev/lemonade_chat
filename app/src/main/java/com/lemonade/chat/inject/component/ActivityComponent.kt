package com.lemonade.chat.inject.component

import dagger.Component
import com.lemonade.chat.MainActivity
import com.lemonade.chat.inject.modules.AppModule
import javax.inject.Singleton

@Component(modules = [(AppModule::class)])
@Singleton
interface ActivityComponent {
    fun inject(activity: MainActivity)
}
