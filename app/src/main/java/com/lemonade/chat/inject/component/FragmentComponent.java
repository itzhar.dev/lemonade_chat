package com.lemonade.chat.inject.component;

import javax.inject.Singleton;

import dagger.Component;
import com.lemonade.chat.fragments.MainFragment;

import com.lemonade.chat.inject.modules.AppModule;

import com.lemonade.chat.data.repositories.LemonadeRepository;

@Component(modules = {AppModule.class})
@Singleton
public interface FragmentComponent {
    void inject(MainFragment fragment);
    LemonadeRepository provideLemonadeepository();
}
