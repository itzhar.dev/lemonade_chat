package com.lemonade.chat.inject.modules

import android.app.Application
import android.content.Context

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private var mApplication: Application) {

    @Provides
    @Singleton
    internal fun provideApplication(): Application = mApplication

    @Singleton
    @Provides
    internal fun provideContext(): Context = mApplication
}
