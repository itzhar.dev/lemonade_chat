package com.lemonade.chat.inject.component

import javax.inject.Singleton

import dagger.Component
import com.lemonade.chat.LemonadeApplication
import com.lemonade.chat.inject.modules.AppModule

@Singleton
@Component(modules = [(AppModule::class)])
interface ApplicationComponent {
    fun inject(application: LemonadeApplication)
}